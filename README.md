# dsblocks

Modular status monitor for dwm, written and meant to be configured in C, with
features including signaling, clickability, cursor hinting and color.

# Installation

`sudo make clean install`

# Usage

`dsblocks`

# Modifying blocks

Each block has two functions associated with it, one responsible for updating
what is shown in status and the other responsible for handling clicks. Take a
look at [config.h](config.h) and files in [blocks](blocks) folder. Functions
defined in [util.c](util.c) might prove helpful when adding your own blocks.

# Clickability

[patches](patches) folder contains two patches for dwm, one for dwm already
patched with systray patch and the other for vanilla dwm. One of the patches,
whichever appropriate, is essential for dsblocks to function properly. It will
add support for colored text (inspired by statuscolors patch for dwm),
clickability (inspired by statuscmd patch for dwm) and cursor hinting when
hovering on clickable blocks (inspired by polybar).

# Signaling changes

To signal a specific block to update, run `sigdsblocks <signal> [<sigval>]`.
`<sigval>` is optional and must be an integer.

It is possible to signal inside a block with `csigself' function described in [util.h](util.h).

# xgetrootname

A tiny program to get the current root name. May prove helpful in debugging.

# Making this build work

## Scripts

Make sure to modify SCRIPT macro inside [util.h](util.h) so that it points to your home folder. Then put inside said folder the necessary scripts included in the [helpers/scripts](helpers/scripts) directory of this project. 

## Fonts

This fonts need to installed and included in your dwm config file.

- JuliaMono
- SauceCodeProNFM
- Hack
- JoyPixels

## Programs

Clickability of the blocks may look for this applications:

- htop
- st
- orage
- pavucontrol
- libnotify (with a corresponding notification server)
- nm-connection-editor

You can disable those calls easy. For example, for the ram block just go to [blocks/ram.c](ram.c) file, inside ramc function you can see what each mouse button does.
