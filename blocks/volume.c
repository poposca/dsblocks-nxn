#include <stdio.h>

#include "../util.h"
#include "volume.h"

/* 
  Speaker normal-muted
  Headphones normal-muted
*/
#define ICONsn                          COL1 "🔊" COL0
#define ICONsm                          COL2 "🔇" COL0
#define ICONhn                          COL1 "🎧" COL0
#define ICONhm                          COL2 "📵" COL0

#define PULSEINFO                       (char *[]){ SCRIPT("pulse_info.pipewire.sh"), NULL }

#define PAVUCONTROL                     (char *[]){ "pavucontrol", NULL }
#define NORMALIZEVOLUME                 (char *[]){ SCRIPT("pulse_normalize.pipewire.sh"), NULL }
#define TOGGLEMUTE                      (char *[]){ "pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle", NULL }

size_t
volumeu(char *str, int sigval)
{
        static char *icons[] = { ICONsn, ICONsm, ICONhn, ICONhm };
        char buf[32];
        size_t l;

        if (!(l = getcmdout(PULSEINFO, buf, sizeof buf - 1))) {
                *str = '\0';
                return 1;
        }
        buf[l] = '\0';
        size_t text_len = SPRINTF(str, "%s %s", icons[buf[0] - '0'], buf + 1);
        return text_len;
}

void
volumec(int button)
{
        switch(button) {
                case 1:
                        if ( fork()==0) {
                            cspawn(TOGGLEMUTE);
                        } else {
                            //printf("Calling csigself... \n");
                            csigself(2, 0);
                        }
                        break;
                case 2:
                        cspawn(NORMALIZEVOLUME);
                        break;
                case 3:
                        cspawn(PAVUCONTROL);
                        break;
        }
}
