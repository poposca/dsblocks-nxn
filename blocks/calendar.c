#include <time.h>
#include <locale.h>
#include <stdio.h>

#include "../util.h"
#include "calendar.h"

#define ICON                    COL1 "" COL0

#define TOGGLECALCURSE          (char *[]){ SCRIPT("sigdwm"), "scrt i 4", NULL }
/* Adjust according to your terminal or your st default font-size (-g option) */
#define CALSINGLE               (char *[]) { "st", "-g", "21X8-1", "-i", "-e", "sh", "-c", "cal;read", NULL}
#define CALTRIPLE               (char *[]) { "st", "-g", "64X9-1", "-i", "-e", "sh", "-c", "cal -3;read", NULL}

size_t
calendaru(char *str, int sigval)
{
        static int hidetime = 0;
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);

        setlocale(LC_TIME, "es_EC.utf-8");
        //char* current_locale = getlocale(LC_TIME);
        // printf("%s \n", current_locale);

        if (sigval == 0)
                hidetime = !hidetime;

        if (hidetime)
                return strftime(str, BLOCKLENGTH, ICON " %a %b %d", &tm) + 1;
        else
                return strftime(str, BLOCKLENGTH, ICON " %a, %b %d %H:%M", &tm) + 1;
}

void
calendarc(int button)
{
        switch (button) {
                case 1:
                        cspawn(CALSINGLE);
                        break;
                case 2:
                        cspawn(CALTRIPLE);
                        break;
                case 3:
                        csigself(1, 0);
                        break;
        }
}
